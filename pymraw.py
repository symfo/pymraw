# -*- coding:utf-8 -*-

import io
import subprocess

class Imagick(object):
    
    def __init__(self, blob=None):
        self.destroy()
        self.read_blob(blob)

    def read_blob(self, blob):
        self._blob = blob

    def get_blob(self):
        return self._blob

    def width(self):
        self._identify()
        return self._size['width']

    def height(self):
        self._identify()
        return self._size['height']

    def exif(self, key):
        self._identify()
        return self._exif.get(key, None)


    def resize(self, width, height, strip=True, quality=0):
        
        opt = ''
        if strip:
           opt += ' -strip'
        if quality:
            opt += ' -quality %(quality)d' % {'quality':quality}

        cmd = 'convert - -resize %(width)dx%(height)d %(opt)s -' % {'width':width, 'height':height, 'opt':opt}
        outs = self._exec(cmd)
        return Imagick(outs)


    def rotate(self, degrees):

        cmd = 'convert - -rotate %(degrees)d -' % {'degrees':degrees}
        outs = self._exec(cmd)
        return Imagick(outs)


    def crop(self, width, height, x, y):

        param = {'width':width, 'height':height, 'x':x, 'y':y}
        cmd = 'convert - -crop %(width)dx%(height)d+%(x)d+%(y)d -' % param
        outs = self._exec(cmd)
        return Imagick(outs)

    def composite(self, water_mark_file):
        
        cmd = 'composite -tile %(water_mark_file)s - -' % {'water_mark_file':water_mark_file}
        outs = self._exec(cmd)
        return Imagick(outs)


    def strip(self):
        cmd = 'convert - -strip -'
        outs = self._exec(cmd)
        return Imagick(outs)

    def destroy(self):
        self._blob = None
        self._size = {'width':0, 'height':0}
        self._exif = {}
        self._identify_read = False



    def _identify(self):
        if self._identify_read:
            return

        cmd = 'identify -format "width:%w\nheight:%h\n%[exif:*]" -'
        outs = self._exec(cmd)
        output = outs.decode('utf-8')

        for line in output.split('\n'):
            info = line.strip().split(':', maxsplit=1)
            if len(info) < 2:
                continue
            
            if info[0] in self._size:
                self._size[info[0]] = int(info[1])
                continue

            # exif
            exif_pair = info[1].split('=', maxsplit=1)
            self._exif[exif_pair[0]] = exif_pair[1]

        self._identify_read = True


    def _exec(self, cmd):
        proc = subprocess.Popen(
            cmd,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            shell=True
        )

        outs, errs = proc.communicate(self._blob)
        return outs


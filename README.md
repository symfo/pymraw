# README #

ImageMagickがインストールされた環境で、convertコマンド等を直接実行してリサイズを実行します。
ライブラリに依存せず、1ファイル単体で動作します。
AWS Lambdaで使用するために作成しました。

### How do I get set up? ###

pymraw.pyをダウンロードして、ソースコードと同じ位置に保存。


    # -*- coding:utf-8 -*-
    import os
    import pymraw

    base_dir = os.path.abspath(os.path.dirname(__file__))

    with  open(os.path.join(base_dir, 'sample.jpg'), 'rb') as f:
        img = f.read()

    m = pymraw.Imagick(img)
    # m.read_blob(img)

    print(m.width())
    print(m.height())
    print(m.exif('DateTimeOriginal'))

    r = m.resize(300, 300).rotate(90)
    r = m.crop(300, 600, 0, 20)
    r = m.composite(os.path.join(base_dir, 'lace_round_5.png'))
    with open('test-convert.jpg', 'wb') as o:
        o.write(r.get_blob())



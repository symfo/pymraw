# -*- coding:utf-8 -*-

import os
import sys
import unittest

sys.path.append('../')
import pymraw

"""
sample1.jpg
    http://www.exiv2.org/sample.html

lace_round_5.png
    http://www.g-floor.org/archives/2087
"""

class TestPymraw(unittest.TestCase):

    def setUp(self):
        with open('sample1.jpg', 'rb') as f:
            self.blob = f.read()

    def test_load(self):

        im = pymraw.Imagick(self.blob)
        self.assertEqual(im.get_blob(), self.blob)

        im2 = pymraw.Imagick()
        im2.read_blob(self.blob)
        self.assertEqual(im2.get_blob(), self.blob)

    def test_identify(self):

        im = pymraw.Imagick(self.blob)
        self.assertEqual(im.width(), 480)
        self.assertEqual(im.height(), 360)
        self.assertEqual(im.exif('DateTimeOriginal'), '2003:12:14 12:01:44')

    def test_resize(self):

        im = pymraw.Imagick(self.blob)
        resize = im.resize(240, 240)
        self.assertEqual(resize.width(), 240)
        self.assertEqual(resize.height(), 180)
        self.assertEqual(resize.exif('DateTimeOriginal'), None)

    def test_resize_no_strip(self):

        im = pymraw.Imagick(self.blob)
        resize = im.resize(240, 240, False)
        self.assertEqual(resize.width(), 240)
        self.assertEqual(resize.height(), 180)
        self.assertEqual(resize.exif('DateTimeOriginal'), im.exif('DateTimeOriginal'))


    def test_rotate(self):

        im = pymraw.Imagick(self.blob)
        resize = im.rotate(90)
        self.assertEqual(resize.width(), 360)
        self.assertEqual(resize.height(), 480)


    def test_crop(self):

        im = pymraw.Imagick(self.blob)
        crop = im.crop(120, 100, 10, 20)
        self.assertEqual(crop.width(), 120)
        self.assertEqual(crop.height(), 100)

    def test_composite(self):

        im = pymraw.Imagick(self.blob)

        base_dir = os.path.abspath(os.path.dirname(__file__))
        water_mark = os.path.join(base_dir, 'lace_round_5.png')
        composite = im.composite(water_mark)
        self.assertNotEqual(composite.get_blob(), im.get_blob())



if __name__ == '__main__':
    unittest.main()

